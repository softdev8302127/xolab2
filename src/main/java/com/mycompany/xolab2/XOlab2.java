/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.xolab2;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class XOlab2 {

    private static char[][] board = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private static char currentPlayer = 'X';
    private static int row;
    private static int col;
     private static String isContinue;
    public static void main(String[] args) {
        printWelcome();
        while (true) {
            printTable();
            printTurn();

            if (isWin()) {
                printTable();
                printWin();
                break;

            } else if (checkDraw()) {
                printTable();
                printDraw();
            }
            swapPlayer();
        }
         inputContinue();

    }

    private static void printWelcome() {
        System.out.println("Welcome To OX Game");
    }

    private static void printTable() {
        System.out.println("-------------");
        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " | ");
            }
            System.out.println("");
        }
        System.out.println("-------------");

    }

    private static void printTurn() {
        Scanner kb = new Scanner(System.in);
        while (true) {
            System.out.print(currentPlayer + " turn. Please input row[1-3] and column[1-3]: ");
            row = kb.nextInt();
            col = kb.nextInt();
            if (row >=0 && row <3 && col >= 0 &&col < 3 &&board[row][col]== '-'  ) {
                board[row - 1][col - 1] = currentPlayer;
                return;
            }
        }
    }

    private static void swapPlayer() {
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
    }

    private static boolean isWin() {
        if (checkRow() || checkCol() || checkX1() || checkX2()) {
            return true;
        }
        return false;
    }

    private static void printWin() {
        System.out.println("Congratulations! Player " + currentPlayer + " Win!");
    }

    private static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (board[row - 1][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (board[i][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX1() {
        if (board[0][0] == currentPlayer && board[1][1] == currentPlayer && board[2][2] == currentPlayer) {
            return true;
        }
        return false;

    }

    private static boolean checkX2() {
        if (board[0][2] == currentPlayer && board[1][1] == currentPlayer && board[2][0] == currentPlayer) {
            return true;
        }
        return false;

    }

    private static boolean checkDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    private static void printDraw() {
        System.out.println("Draw!!");
    }

    private static void inputContinue() {
         Scanner kb = new Scanner(System.in);
       while (true) {
           System.out.print("Do you want to play again? (y/n) :");
           isContinue = kb.next();
           if(isContinue.equalsIgnoreCase("y")){
               resetGame();
               main(null);
               break;
              } else if (isContinue.equalsIgnoreCase("n")) {
                System.out.println("Thank you for playing! Goodbye!");
                break;
            } else {
                System.out.println("Invalid input. Please enter 'y' or 'n'.");
            }
        }

    }

    private static void resetGame() {
         board = new char[][]{{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
        currentPlayer = 'X';

    }


}
